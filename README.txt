Information about the Files on this Directory:

BA_Figures :  all figures created by Carina Schröder for the Bachelors-thesis
Bachelorarbeit.pdf : digital Bachelor-thesis
Data : used data for the Bachelor-thesis
WSHPackage-Extension-Files : R scripts changed or created for the extension of the WSHPackage-master
WSHPackage-master : R-project from Michael Scherer (WSH) with extensions made for this Bachelor-thesis
