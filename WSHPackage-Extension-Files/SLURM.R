########################################################################################################################
## SLURM.R
## created: 2022-05-12
## creator: Carina Schroeder
## ---------------------------------------------------------------------------------------------------------------------
## Script used for calling in the shell
########################################################################################################################

library(WSH)
library(GenomicRanges)

# Get command line arguments
args = commandArgs()
sample_name <- as.character(args[6])
chromosome <- as.character(args[7])
bam_file <- args[8]
output_path <- args[9]

# input preparation for score calculation
merged_MA3 <- read.table("/nfs/data/plant_DNAm_heterogeneity/merged_MA3.txt", sep="\t", header=TRUE)

new_annotation <- merged_MA3[merged_MA3$context != "CHH",]
new_annotation <- new_annotation[new_annotation$seqnames!="C",]
new_annotation <- new_annotation[new_annotation$seqnames!="M",]
new_annotation <- new_annotation[new_annotation$seqnames==chromosome,] # use only one chromosome
ranges_obj <- GRanges(seqnames=new_annotation$seqnames, ranges=IRanges(start=new_annotation$start, width=nchar(new_annotation$context))) 
seqlevelsStyle(ranges_obj) <- 'ucsc'


# calculate the scores
start_time <- Sys.time()

pdr <- compute.score(bam.file=bam_file,ranges_obj,score="pdr")
t <- paste(sample_name, "Chr", chromosome, "pdr.txt", sep="_", collapse=NULL)
write.table(pdr, file=paste(output_path, t, sep=""), sep="\t", eol = "\n", na = "NA", dec = ".", row.names = TRUE, col.names = TRUE, quote = FALSE)

fdrp <- compute.score(bam.file=bam_file,ranges_obj,score="fdrp")
t <- paste(sample_name, "Chr", chromosome, "fdrp.txt", sep="_", collapse=NULL)
write.table(fdrp, file=paste(output_path, t, sep=""), sep="\t", eol = "\n", na = "NA", dec = ".", row.names = TRUE, col.names = TRUE, quote = FALSE)

qfdrp <- compute.score(bam.file=bam_file,ranges_obj,score="qfdrp")
t <- paste(sample_name, "Chr", chromosome, "qfdrp.txt", sep="_", collapse=NULL)
write.table(qfdrp, file=paste(output_path, t, sep=""), sep="\t", eol = "\n", na = "NA", dec = ".", row.names = TRUE, col.names = TRUE, quote = FALSE)

epipoly <- compute.score(bam.file=bam_file,score="epipolymorphism")
t <- paste(sample_name, "Chr", chromosome, "epipoly.txt", sep="_", collapse=NULL)
write.table(epipoly, file=paste(output_path, t, sep=""), sep="\t", eol = "\n", na = "NA", dec = ".", row.names = TRUE, col.names = TRUE, quote = FALSE)

entropy <- compute.score(bam.file=bam_file,score="entropy")
t <- paste(sample_name, "Chr", chromosome, "entropy.txt", sep="_", collapse=NULL)
write.table(entropy, file=paste(output_path, t, sep=""), sep="\t", eol = "\n", na = "NA", dec = ".", row.names = TRUE, col.names = TRUE, quote = FALSE)

end_time <- Sys.time()
end_time - start_time

