########################################################################################################################
## Scores_Data.R
## created: 2022-05-12
## creator: Carina Schroeder
## ---------------------------------------------------------------------------------------------------------------------
## Script with all data for each score
########################################################################################################################


#' Sample Col_G1_L8
pdr_18_chr1 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G1_L8_Chr_1_pdr.txt", sep="\t", header=TRUE)
pdr_18_chr2 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G1_L8_Chr_2_pdr.txt", sep="\t", header=TRUE)
pdr_18_chr3 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G1_L8_Chr_3_pdr.txt", sep="\t", header=TRUE)
pdr_18_chr4 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G1_L8_Chr_4_pdr.txt", sep="\t", header=TRUE)
pdr_18_chr5 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G1_L8_Chr_5_pdr.txt", sep="\t", header=TRUE)

# fdrp_18_chr1 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G1_L8_Chr_1_fdrp.txt", sep="\t", header=TRUE)
fdrp_18_chr2 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G1_L8_Chr_2_fdrp.txt", sep="\t", header=TRUE)
fdrp_18_chr3 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G1_L8_Chr_3_fdrp.txt", sep="\t", header=TRUE)
fdrp_18_chr4 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G1_L8_Chr_4_fdrp.txt", sep="\t", header=TRUE)
fdrp_18_chr5 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G1_L8_Chr_5_fdrp.txt", sep="\t", header=TRUE)

# qfdrp_18_chr1 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G1_L8_Chr_1_qfdrp.txt", sep="\t", header=TRUE)
qfdrp_18_chr2 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G1_L8_Chr_2_qfdrp.txt", sep="\t", header=TRUE)
qfdrp_18_chr3 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G1_L8_Chr_3_qfdrp.txt", sep="\t", header=TRUE)
qfdrp_18_chr4 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G1_L8_Chr_4_qfdrp.txt", sep="\t", header=TRUE)
qfdrp_18_chr5 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G1_L8_Chr_5_qfdrp.txt", sep="\t", header=TRUE)

epipoly_18 <- read.delim("~/WSHPackage-master/files_Carina/epipoly_entropy/epipoly_18.txt", sep="\t", header=TRUE)
entropy_18 <- read.delim("~/WSHPackage-master/files_Carina/epipoly_entropy/entropy_18.txt", sep="\t", header=TRUE)

#' Sample Col_G1_L2
pdr_12_chr1 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G1_L2_Chr_1_pdr.txt", sep="\t", header=TRUE)
pdr_12_chr2 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G1_L2_Chr_2_pdr.txt", sep="\t", header=TRUE)
pdr_12_chr3 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G1_L2_Chr_3_pdr.txt", sep="\t", header=TRUE)
pdr_12_chr4 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G1_L2_Chr_4_pdr.txt", sep="\t", header=TRUE)
pdr_12_chr5 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G1_L2_Chr_5_pdr.txt", sep="\t", header=TRUE)

# fdrp_12_chr1 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G1_L2_Chr_1_fdrp.txt", sep="\t", header=TRUE)
fdrp_12_chr2 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G1_L2_Chr_2_fdrp.txt", sep="\t", header=TRUE)
fdrp_12_chr3 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G1_L2_Chr_3_fdrp.txt", sep="\t", header=TRUE)
fdrp_12_chr4 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G1_L2_Chr_4_fdrp.txt", sep="\t", header=TRUE)
# fdrp_12_chr5 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G1_L2_Chr_5_fdrp.txt", sep="\t", header=TRUE)

# qfdrp_12_chr1 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G1_L2_Chr_1_qfdrp.txt", sep="\t", header=TRUE)
# qfdrp_12_chr2 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G1_L2_Chr_2_qfdrp.txt", sep="\t", header=TRUE)
# qfdrp_12_chr3 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G1_L2_Chr_3_qfdrp.txt", sep="\t", header=TRUE)
qfdrp_12_chr4 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G1_L2_Chr_4_qfdrp.txt", sep="\t", header=TRUE)
# qfdrp_12_chr5 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G1_L2_Chr_5_qfdrp.txt", sep="\t", header=TRUE)

epipoly_12 <- read.delim("~/WSHPackage-master/files_Carina/epipoly_entropy/epipoly_12.txt", sep="\t", header=TRUE)
entropy_12 <- read.delim("~/WSHPackage-master/files_Carina/epipoly_entropy/entropy_12.txt", sep="\t", header=TRUE)

# Sample Col_G2_L2
pdr_22_chr1 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G2_L2_Chr_1_pdr.txt", sep="\t", header=TRUE)
pdr_22_chr2 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G2_L2_Chr_2_pdr.txt", sep="\t", header=TRUE)
pdr_22_chr3 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G2_L2_Chr_3_pdr.txt", sep="\t", header=TRUE)
pdr_22_chr4 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G2_L2_Chr_4_pdr.txt", sep="\t", header=TRUE)
pdr_22_chr5 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G2_L2_Chr_5_pdr.txt", sep="\t", header=TRUE)

# fdrp_22_chr1 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G2_L2_Chr_1_fdrp.txt", sep="\t", header=TRUE)
fdrp_22_chr2 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G2_L2_Chr_2_fdrp.txt", sep="\t", header=TRUE)
fdrp_22_chr3 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G2_L2_Chr_3_fdrp.txt", sep="\t", header=TRUE)
fdrp_22_chr4 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G2_L2_Chr_4_fdrp.txt", sep="\t", header=TRUE)
# fdrp_22_chr5 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G2_L2_Chr_5_fdrp.txt", sep="\t", header=TRUE)

# qfdrp_22_chr1 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G2_L2_Chr_1_qfdrp.txt", sep="\t", header=TRUE)
# qfdrp_22_chr2 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G2_L2_Chr_2_qfdrp.txt", sep="\t", header=TRUE)
# qfdrp_22_chr3 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G2_L2_Chr_3_qfdrp.txt", sep="\t", header=TRUE)
# qfdrp_22_chr4 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G2_L2_Chr_4_qfdrp.txt", sep="\t", header=TRUE)
# qfdrp_22_chr5 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G2_L2_Chr_5_qfdrp.txt", sep="\t", header=TRUE)

epipoly_22 <- read.delim("~/WSHPackage-master/files_Carina/epipoly_entropy/epipoly_22.txt", sep="\t", header=TRUE)
entropy_22 <- read.delim("~/WSHPackage-master/files_Carina/epipoly_entropy/entropy_22.txt", sep="\t", header=TRUE)

# Sample Col_G2_L8
pdr_28_chr1 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G2_L8_Chr_1_pdr.txt", sep="\t", header=TRUE)
pdr_28_chr2 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G2_L8_Chr_2_pdr.txt", sep="\t", header=TRUE)
pdr_28_chr3 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G2_L8_Chr_3_pdr.txt", sep="\t", header=TRUE)
pdr_28_chr4 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G2_L8_Chr_4_pdr.txt", sep="\t", header=TRUE)
pdr_28_chr5 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G2_L8_Chr_5_pdr.txt", sep="\t", header=TRUE)

# fdrp_28_chr1 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G2_L8_Chr_1_fdrp.txt", sep="\t", header=TRUE)
fdrp_28_chr2 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G2_L8_Chr_2_fdrp.txt", sep="\t", header=TRUE)
fdrp_28_chr3 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G2_L8_Chr_3_fdrp.txt", sep="\t", header=TRUE)
fdrp_28_chr4 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G2_L8_Chr_4_fdrp.txt", sep="\t", header=TRUE)
# fdrp_28_chr5 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G2_L8_Chr_5_fdrp.txt", sep="\t", header=TRUE)

# qfdrp_28_chr1 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G2_L8_Chr_1_qfdrp.txt", sep="\t", header=TRUE)
# qfdrp_28_chr2 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G2_L8_Chr_2_qfdrp.txt", sep="\t", header=TRUE)
# qfdrp_28_chr3 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G2_L8_Chr_3_qfdrp.txt", sep="\t", header=TRUE)
# qfdrp_28_chr4 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G2_L8_Chr_4_qfdrp.txt", sep="\t", header=TRUE)
# qfdrp_28_chr5 <- read.delim("~/WSHPackage-master/files_Carina/output_files/Col_G2_L8_Chr_5_qfdrp.txt", sep="\t", header=TRUE)

epipoly_28 <- read.delim("~/WSHPackage-master/files_Carina/epipoly_entropy/epipoly_28.txt", sep="\t", header=TRUE)
entropy_28 <- read.delim("~/WSHPackage-master/files_Carina/epipoly_entropy/entropy_28.txt", sep="\t", header=TRUE)




