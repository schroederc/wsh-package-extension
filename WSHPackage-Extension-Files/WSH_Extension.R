########################################################################################################################
## WSH_Extension.R
## created: 2022-05-13
## creator: Carina Schroeder
## ---------------------------------------------------------------------------------------------------------------------
## Extension of the WSH package: additional functions and scripts for the calculation and analysis of the WSH-Scores
## Includes the modifications to be implemented into the WSH package for the analysis of CG vs CHG methylated positions
########################################################################################################################

#' plot.coverage.distribution 
#' 
#' function to plot the coverage Distribution and the mean coverage (indicated in red and stated in blue) of a given sample. 
#' This can give an first insight into the annotation.
#' 
#' @param annotation annotation with the coverage info rmoation of the different samples
#' @param sample_name name of the sample that should be analysed
#' 
#' @return a distribution plot
#' 
#' @author Carina Schroeder
#' 
#' @noRd

plot.coverage.distribution <- function(annotation, sample_name){
  sample_name <- deparse(substitute(sample_name))   # get Name of function argument
  p <- ggplot(annotation, aes(x=annotation[,sample_name]), xlim=c(0,50)) + 
    geom_histogram(binwidth=1, color="black", fill="white") + 
    geom_vline(aes(xintercept=mean(annotation[,sample_name], na.rm=T)),color="red", linetype="dashed", size=1) + 
    geom_text(aes(x=100, label=mean(annotation[,sample_name], na.rm=T), y=3e+05), colour="blue", vjust = 1.2)
  p + labs(x = sample_name, title = paste("Coverage Distribution for sample: ", sample_name, sep=""))
}

# example input
plot.coverage.distribution(annotation, Col_G5_L2)



#' calculate.Scores
#' 
#' This function calculates the scores PDR, FDRP, qFRRP, Epipoly and Entropy for a bam_file and ranges_obj.
#' The results are saved in separate files for later analyzing and visualization (plots). This function here was for test purposes.
#' The Script where the scores were calculated is SLURM.R.
#' 
#' @param bam_file bam file with the reads from a bisulfite sequencing technique already aligned to a reference genome (übernommen)
#' @param ranges_obj annotation as a GRanges object with the CpG (and CHH?) sites to be analyzed (übernommen)
#' 
#' @return output files with scores
#' 
#' @author Carina Schroeder
#' 
#' @export

calculate.Scores <- function(bam_file, ranges_obj){
  
  start_time <- Sys.time()
  pdr <- compute.score(bam.file=bam_file,ranges_obj,score="pdr")
  end_time <- Sys.time()
  end_time - start_time
  write.table(pdr, file="~/WSHPackage-master/files_Carina/files_for_CHH/output_files/pdr_test.txt", sep="\t", eol = "\n", na = "NA", dec = ".", row.names = TRUE, col.names = TRUE, quote = FALSE)
  
  start_time <- Sys.time()
  fdrp <- compute.score(bam.file=bam_file,ranges_obj,score="fdrp")
  end_time <- Sys.time()
  end_time - start_time
  write.table(fdrp, file="~/WSHPackage-master/files_Carina/files_for_CHH/output_files/fdrp_test.txt", sep="\t", eol = "\n", na = "NA", dec = ".", row.names = TRUE, col.names = TRUE, quote = FALSE)
  
  
  start_time <- Sys.time()
  qfdrp <- compute.score(bam.file=bam_file,ranges_obj,score="qfdrp")
  end_time <- Sys.time()
  end_time - start_time
  write.table(qfdrp, file="~/WSHPackage-master/files_Carina/files_for_CHH/output_files/qfdrp_test.txt", sep="\t", eol = "\n", na = "NA", dec = ".", row.names = TRUE, col.names = TRUE, quote = FALSE)
  
  
  start_time <- Sys.time()
  epipoly <- compute.score(bam.file=bam_file,score="epipolymorphism")
  end_time <- Sys.time()
  end_time - start_time
  write.table(epipoly, file="~/WSHPackage-master/files_Carina/files_for_CHH/output_files/epipoly_test.txt", sep="\t", eol = "\n", na = "NA", dec = ".", row.names = TRUE, col.names = TRUE, quote = FALSE)
  
  
  start_time <- Sys.time()
  entropy <- compute.score(bam.file=bam_file,score="entropy")
  end_time <- Sys.time()
  end_time - start_time
  write.table(entropy, file="~/WSHPackage-master/files_Carina/files_for_CHH/output_files/entropy_test.txt", sep="\t", eol = "\n", na = "NA", dec = ".", row.names = TRUE, col.names = TRUE, quote = FALSE)
  
}

# Example
calculate.Scores(bam_file = "/nfs/data/plant_DNAm_heterogeneity/Col_G1_L8-merged_sorted.bam", ranges_objM)



#' plot.scores.distribution
#' 
#' function to plot the distribution of the scores without NA-entries and also returns the 75% quantile and the number of zero-entries
#' 
#' @param score score as calculated through compute.score.GRanges
#' @param ignore_zeroes if TRUE then don't consider zero-entries
#' @param scipen if TRUE use scientific notation (e+ values), otherwise prevent scientific notation
#' 
#' @return disctribution plot and quantile information
#' 
#' @details in addition to the distribution plot this function also returns the quantile information as standard output
#' 
#' @author Carina Schroeder
#' 
#' @export

plot.scores.distribution <- function(score, ignore_zeroes=FALSE, scipen=FALSE) { 
  
  funcarg <- deparse(substitute(score)) # get name of function argument
  
  if(ignore_zeroes == TRUE){   # if ignore_zeroes = TRUE: zero-entries are replaced with NA
    score[score == 0] <- NA # remove all 0 from data
  } else {
    zeroes <- length(which(score[,ncol(score)] == 0)) # count how many 0-entries were in the score
  }
  
  if(scipen == TRUE){
    options(scipen = 0, digits = 4)
  } else {
    options(scipen = 100, digits = 4) # changes R settings to prevent scientific notation (e+ values)
  }
  
  # get quantile info
  z <- quantile(score[,ncol(score)], na.rm = TRUE) # remove all na from data and print quantile info
  print(z)
  prob <- c(0.0, 0.25, 0.5, 0.75, 1.0) # the quantile-probabilities we want to have
  quantile_df <- data.frame(id = prob, values = z) # make quantile info from named vector into dataframe
  
  # create the plot
  main_text <- paste(unlist(c("Distribution of", tail(names(score),1), "scores")), collapse=' ') # title of histogram
  x_text <- tail(names(score),1) # x axis of histogram
  h <- hist(score[,ncol(score)], main = main_text, xlab = x_text, cex.main=1.5, cex.lab=1.7, cex.axis=1.3) # cex.main = font size of the title
  title(line=1)
  text(h$mids,h$counts,labels=h$counts, adj=c(0.5, -0.5), cex=1.0)
  
  # add quantile info for the 75% quantile to the plots
  if(ignore_zeroes == TRUE){
    t1 <- paste("75% quantile = ", round(quantile_df[4,2], digits = 3), sep="")
    mtext(t1, cex=1.3, line=-1)
  } else {
    t1 <- paste("75% quantile = ", round(quantile_df[4,2], digits = 3), sep="")
    t2 <- paste("0-entries =", zeroes, sep=" ")
    mtext(paste(t1, t2, sep="\n"), cex=1.3, line=-1)
  }
  
}

# layout(mat = matrix(c(1,1,2,2,3,3,0,4,4,5,5,0), nrow = 2, byrow = TRUE)) # if you want all five scores distribution in one plot
par(mar=c(5,6,4,1)+.01) # so the label isn't cut off
par(mfrow=c(1,1)) # to return to normal layout -> only one plot

# example
plot.scores.distribution(fdrp_18_chr1)


#' get.scores.info
#' 
#' get the quantile information and number of zeroes, NAs, and number of all scores
#' 
#' @param score score as calculated through compute.score.GRanges
#' @param ignore_zeroes if TRUE then don't consider zero-entries in the quantile-calculation 
#' 
#' @return name of Score with the quantile information and information of number of zeroes, NAs and number of all 
#' scores printed on thecommand line
#' 
#' @details this function, same as plot.scores.distribution gives a very quick insight into the scores, 
#' but is faster since no plot has to be developed
#' 
#' @author Carina Schroeder
#' 
#' @export

get.scores.info <- function(score, ignore_zeroes=FALSE){
  
  funcarg <- deparse(substitute(score)) # get Name of function argument
  
  if(ignore_zeroes == TRUE){
    zeroes <- length(which(score[,ncol(score)] == 0)) # count how many 0-entries were in the score
    notavailables <- sum(is.na(score[,ncol(score)])) # count how many NAs are in the score
    score[score == 0] <- NA # remove all 0 from data
  } else {
    zeroes <- length(which(score[,ncol(score)] == 0)) # count how many 0-entries were in the score
    notavailables <- sum(is.na(score[,ncol(score)])) # count how many NAs are in the score
  }
  
  z <- quantile(score[,ncol(score)], na.rm = TRUE) # remove all na from data and print quantile info
  prob <- c(0.0, 0.25, 0.5, 0.75, 1.0) # the probabilities we want to have
  quantile_df <- data.frame(id = prob, values = z) # make quantile info from named vector into dataframe
  
  # get extra information about the scores
  all <- length(score[,ncol(score)])
  
  cat("Score \t 0% \t 25% \t 50% \t 75% \t 100% \t #Zeroes \t #NAs \t #Scores \n")
  cat(c(funcarg, "\t", round(quantile_df[1,2], digits = 4), "\t", round(quantile_df[2,2], digits = 4), "\t", round(quantile_df[3,2], digits = 4), "\t", round(quantile_df[4,2], digits = 4), "\t", round(quantile_df[5,2], digits = 4), "\t", zeroes, "\t", notavailables, "\t", all))
}

# example
get.scores.info(pdr_12_chr1, ignore_zeroes = TRUE)


#' count.occurences
#'
#' function used in compare.contexts. Returns the mean of all scores (hits) for a window if they are at least min.score
#' 
#' @param hits scores for a context in the defined window (from compare.contexts) 
#' @param min.score Minimum allowed CHGs and CpGs per window. If the window has not enough CpGs and CHGs, it won't be considered further
#' 
#' @return mean of all hits if the number of hits is >= min.score
#' 
#' @author Carina Schroeder
#' 
#' @noRD

count.occurences <- function(hits, min.score){
  if(length(hits) >= min.score){
    return(mean(hits, na.rm=TRUE))
  } else {
    return(NULL)
  }
}


#' compare.contexts
#'
#' extension of create.genomebrowser.track from main.R (Converts the output of compute.score and 
#' compute.score.GRanges into genome browser tracks, that can be directly imported into e.g. UCSC Genome Browser). 
#' This function compares CpG and CHG.
#'
#' @param score.output The output as generated through compute.score or compute.score.GRanges: a
#'        data.frame with chromosome positions and the score values for those positions.
#' @param bin.width Width of bins in bp to aggregate the scores. If NULL, the scores are stored
#'        as single CpGs.
#' @param min.score Minimum allowed CHGs and CpGs per window. If the bin has not enough CpGs and CHGs, dann 
#'        betrachtet man dieses bin nicht weiter
#'        
#' @author Michael Scherer and Extension from Carina Schroeder
#' 
#' @export

compare.contexts <- function(score.output, bin.width=200, min.score=4){
  if(!is.data.frame(score.output)){
    stop("Invalid value for score.output, needs to be a data.frame")
  }
  if(!all((c("chromosome","start","end") %in% colnames(score.output)))){
    stop("Neccessary column names not available")
  }
  
  score <- colnames(score.output)[5]
  whichChr <- score.output[1,1] 
  if(!is.null(bin.width)){
    agg <- lapply(unique(score.output$chromosome),function(chr){
      start.chr <- min(score.output$start[score.output$chromosome %in% chr])
      end.chr <- max(score.output$end[score.output$chromosome %in% chr])
      cbind(as.character(chr),seq(start.chr,end.chr-bin.width,by=bin.width),seq(start.chr+bin.width,end.chr,by=bin.width))
    })
    agg.frame <- c()
    for(chr in agg){
      agg.frame <- rbind(agg.frame,chr)
    }
    agg.ranges <- GRanges(Rle(agg.frame[,1]),IRanges(start=as.numeric(agg.frame[,2]),as.numeric(agg.frame[,3])))
    score.output.cpgs <- score.output[score.output$context=="CG",]
    cpgs.anno <- GRanges(Rle(score.output.cpgs$chromosome),IRanges(start=score.output.cpgs$start,end=score.output.cpgs$end))
    op.cpgs <- findOverlaps(cpgs.anno,agg.ranges)
    
    score.output.chgs <- score.output[score.output$context=="CHG",]
    chgs.anno <- GRanges(Rle(score.output.chgs$chromosome),IRanges(start=score.output.chgs$start,end=score.output.chgs$end))
    op.chgs <- findOverlaps(chgs.anno,agg.ranges)
    
    # check if there are more than min.score of each context for each window
    agg.score.cpgs <- aggregate(score.output.cpgs[queryHits(op.cpgs),5],by=list(subjectHits(op.cpgs)),FUN=function(x) count.occurences(x, min.score))
    agg.score.chgs <- aggregate(score.output.chgs[queryHits(op.chgs),5],by=list(subjectHits(op.chgs)),FUN=function(x) count.occurences(x, min.score))

    # create output frame 
    output.cpgs <- cbind(agg.frame[unique(subjectHits(op.cpgs)),],agg.score.cpgs$x)
    output.chgs <- cbind(agg.frame[unique(subjectHits(op.chgs)),],agg.score.chgs$x)
    
    # convert matrix into dataframe first
    output.cpgs.asmatrix <- as.data.frame(output.cpgs, ncol=4) 
    names(output.cpgs.asmatrix) <- c("chromosome", "start", "end", "CpG-mean")
    
    output.chgs.asmatrix <- as.data.frame(output.chgs, ncol=4)
    names(output.chgs.asmatrix) <- c("chromosome", "start", "end", "CHG-mean")

    # remove all rows that contain NULL
    output.cpgs.withoutNULL <- output.cpgs.asmatrix[!(output.cpgs.asmatrix$`CpG-mean`=="NULL"), ]
    output.chgs.withoutNULL <- output.chgs.asmatrix[!(output.chgs.asmatrix$`CHG-mean`=="NULL"), ]
    
    output <- merge(output.cpgs.withoutNULL, output.chgs.withoutNULL, by=c("chromosome", "start", "end"))
    score.output <- transform(output, start=as.numeric(start))
    score.output <- transform(score.output, end=as.numeric(end))
    score.output <- score.output[order(score.output$start),]

    plot(score.output$CpG.mean, score.output$CHG.mean, pch=10, cex=0.2, xlab="CpG-Mean", ylab="CHG-mean", main=(paste0(whichChr, ", ", score)), cex.main=1.5, cex.axis=1.3, cex.lab=1.7)
    
    
  }

}

# example
create.genomebrowser.track2(fdrp_18_chr2, bin.width = 1000, min.score = 10)




#' get.gene.mean
#' 
#' function to calculate the mean of the scores for each region
#' (The function name is misleading here, it does not necessarily have to be only genes, it can be any region one wants to look at)
#' 
#' @param score score as calculated through compute.score.GRanges, it's either pdr, fdrp, qfdrp, epipoly or entropy
#' @param gff.genes file for which we want the mean of
#' 
#' @return the modified gff.genes data with additional columns for the mean
#' 
#' @author Carina Schroeder
#' 
#' @noRd 

get.gene.mean = function(score, gff.genes) {
  start_time <- Sys.time()
  mean_vec <- rep(NA, nrow(gff.genes)) # fill an empty Vector where the calculated mean values are stored
  score$chromosome <- gsub("chr", "", score$chromosome)
  lastcol <- colnames(score)[ncol(score)]
  for (i in 1:nrow(gff.genes)) {
    row_gff <- gff.genes[i, ]
    score_vec <- subset(score,(chromosome == row_gff$V1) & (start >= row_gff$V4) & (start < row_gff$V5), select=lastcol)
    score_vec <- score_vec[!is.na(score_vec)] # extract a subset of all positions in the range and then calculate the mean for that range
    x <- mean(score_vec)
    mean_vec[i] <- x
    x <- c()
  }
  gff.genes[,lastcol] <- mean_vec 
  end_time <- Sys.time()
  end_time - start_time
  
  return(gff.genes)
}


#' get.gene.variance
#' 
#' function to calculate the variance of the scores for each region 
#' (The function name is misleading here, it does not necessarily have to be only genes, it can be any region one wants to look at)
#' 
#' @param score score as calculated through compute.score.GRanges, it's either pdr, fdrp, qfdrp, epipoly or entropy
#' @param gff.genes file for which we want the variance of
#' 
#' @return the modified gff.genes data with additional columns for the mean
#' 
#' @author Carina Schroeder
#' 
#' @noRd 

get.gene.variance = function(score, gff.genes) {
  start_time <- Sys.time()
  var_vec <- rep(NA, nrow(gff.genes))
  score$chromosome <- gsub("chr", "", score$chromosome)
  lastcol <- colnames(score)[ncol(score)]
  for (i in 1:nrow(gff.genes)) {
    row_gff <- gff.genes[i, ]
    score_vec <- subset(score,(chromosome == row_gff$V1) & (start >= row_gff$V4) & (start < row_gff$V5), select=lastcol)
    score_vec <- score_vec[!is.na(score_vec)]
    x <- var(score_vec)
    var_vec[i] <- x
    x <- c()
  }
  gff.genes[,lastcol] <- var_vec
  end_time <- Sys.time()
  end_time - start_time
  
  return(gff.genes)
}


#' scatterplot.Mean.Variance
#'
#' plot Mean vs Variance in a Scatterplot, where each dot represents a region
#' 
#' @param pdr score as calculated through compute.score.GRanges
#' @param fdrp score as calculated through compute.score.GRanges
#' @param qfdrp score as calculated through compute.score.GRanges
#' @param epipoly score as calculated through compute.score.GRanges
#' @param entropy score as calculated through compute.score.GRanges
#' @param gff
#' @param context entweder "CG" oder "CHG" oder man schaut sich beide an
#' 
#' @return Scatterplot for each of the 5 scores plotted in one layout. The mean is plotted on the x-axis and the variance on the y-axis.
#' 
#' @details This function is specially adapted to my needs for the paper, therefore all five plots in one, you could have programmed 
#' it so that each plot is seperate, but this way it saved me some time to create each plot. 
#' It is advantageous to adapt the gff file to the calculated scores beforehand, so if you only have the scores for chromosome 3, 
#' it makes sense to shorten the gff file to chromosome 3. This shortens the runtime 
#' 
#' @author Carina Schroeder
#' 
#' @export

scatterplot.mean.variance <- function(pdr, fdrp, qfdrp, epipolymorphism, entropy, gff, context){
  
  if(missing(context)){
    df <- get.gene.mean(pdr, gff)
    df2 <- get.gene.mean(fdrp, df)
    df3 <- get.gene.mean(qfdrp, df2)
    df4 <- get.gene.mean(epipolymorphism, df3)
    file_mean <- get.gene.mean(entropy, df4)
    
    df_2 <- get.gene.variance(pdr, gff)
    df2_2 <- get.gene.variance(fdrp, df_2)
    df3_2 <- get.gene.variance(qfdrp, df2_2)
    df4_2 <- get.gene.variance(epipolymorphism, df3_2)
    file_variance <- get.gene.variance(entropy, df4_2)
  } else {
    df <- get.gene.mean(pdr[pdr$context==context,], gff)
    df2 <- get.gene.mean(fdrp[fdrp$context==context,], df)
    df3 <- get.gene.mean(qfdrp[qfdrp$context==context,], df2)
    df4 <- get.gene.mean(epipolymorphism, df3)
    file_mean <- get.gene.mean(entropy, df4)
    
    df_2 <- get.gene.variance(pdr[pdr$context==context,], gff)
    df2_2 <- get.gene.variance(fdrp[fdrp$context==context,], df_2)
    df3_2 <- get.gene.variance(qfdrp[qfdrp$context==context,], df2_2)
    df4_2 <- get.gene.variance(epipolymorphism, df3_2)
    file_variance <- get.gene.variance(entropy, df4_2)
  }
  
  
  layout(mat = matrix(c(1,1,2,2,3,3,4,4,5,5,0,0), nrow = 2, byrow = TRUE))
  par(mar=c(5,6,4,1)+.1) # so the label isn't cut off
  
  plot(file_mean$PDR, file_variance$PDR, pch=19, cex=0.5, xlab="Mean", ylab="Variance", main="PDR", cex.main=1.5, cex.axis=1.3, cex.lab=1.7)
  plot(file_mean$FDRP, file_variance$FDRP, pch=19, cex=0.5, xlab="Mean", ylab="Variance", main="FDRP", cex.main=1.5, cex.axis=1.3, cex.lab=1.7)
  plot(file_mean$qFDRP, file_variance$qFDRP, pch=19, cex=0.5, xlab="Mean", ylab="Variance", main="qFDRP", cex.main=1.5, cex.axis=1.3, cex.lab=1.7)
  plot(file_mean$Epipolymorphism, file_variance$Epipolymorphism, pch=19, cex=0.5, xlab="Mean", ylab="Variance", main="Epipolymorphism", cex.main=1.5, cex.axis=1.3, cex.lab=1.7)
  plot(file_mean$Entropy, file_variance$Entropy, pch=19, cex=0.5, xlab="Mean", ylab="Variance", main="Entropy", cex.main=1.5, cex.axis=1.3, cex.lab=1.7)
  
}

# Beispiel Aufruf - pro Chromosom aufrufen
# vorher die gff.genes Datei auf das Chromosom cutten -> dann geht es schneller
gff.mRNA.chr3 <- gff.mRNA[gff.mRNA$V1=="3",]
scatterplot.mean.variance(pdr_18_chr3, fdrp_18_chr3, qfdrp_18_chr3, epipoly_18, entropy_18, gff.exon.chr3, "CG")




########################################################################################################################
# EXTRAS -> old stuff, didn't use this

# Gene nach qFDRP Score > 0.1 filtern
gffgenes_18_Mean_cutoff <- gffgenes_18_Mean[!is.na(gffgenes_18_Mean$qFDRP),]
gffgenes_18_Mean_cutoff <- gffgenes_18_Mean_cutoff[gffgenes_18_Mean_cutoff$qFDRP>0.1,]
gffgenes_18_Mean_cutoff$chromosome <- gsub("chr", "", gffgenes_18_Mean_cutoff$chromosome)

gffgenes_52_Mean_cutoff <- gffgenes_52_Mean[!is.na(gffgenes_52_Mean$qFDRP),]
gffgenes_52_Mean_cutoff <- gffgenes_52_Mean_cutoff[gffgenes_52_Mean_cutoff$qFDRP>0.1,]
gffgenes_52_Mean_cutoff$chromosome <- gsub("chr", "", gffgenes_52_Mean_cutoff$chromosome)

write.table(gffgenes_18_Mean_cutoff, file="~/WSHPackage-master/files_Carina/output_files/gffgenes_18_Mean_cutoff.txt", sep="\t", eol = "\n", na = "NA", dec = ".", row.names = FALSE, col.names = TRUE, quote = FALSE, append = TRUE)
write.table(gffgenes_52_Mean_cutoff, file="~/WSHPackage-master/files_Carina/output_files/gffgenes_52_Mean_cutoff.txt", sep="\t", eol = "\n", na = "NA", dec = ".", row.names = FALSE, col.names = TRUE, quote = FALSE, append = TRUE)

# Scores filtern nach qFDRP > 0.1
Col18_1 <- read.delim("~/WSHPackage-master/files_Carina/Col_G1_L8_scores.txt", sep="\t", header=TRUE)
Col18_2 <- read.delim("~/WSHPackage-master/files_Carina/Col_G1_L8_scores2.txt", sep="\t", header=TRUE)
Col52_1 <- read.delim("~/WSHPackage-master/files_Carina/Col_G5_L2_scores.txt", sep="\t", header=TRUE)
Col52_2 <- read.delim("~/WSHPackage-master/files_Carina/Col_G5_L2_scores2.txt", sep="\t", header=TRUE)

Col_18_1_filtered <- Col18_1[!is.na(Col18_1$qFDRP),]
Col_18_1_filtered <- Col_18_1_filtered[Col_18_1_filtered$qFDRP>0.1,]
Col_18_1_filtered$chromosome <- gsub("chr", "", Col_18_1_filtered$chromosome)

Col_52_1_filtered <- Col52_1[!is.na(Col52_1$qFDRP),]
Col_52_1_filtered <- Col_52_1_filtered[Col_52_1_filtered$qFDRP>0.1,]
Col_52_1_filtered$chromosome <- gsub("chr", "", Col_52_1_filtered$chromosome)

write.table(Col_18_1_filtered, file="~/WSHPackage-master/files_Carina/Col_18_1_filtered.txt", sep="\t", eol = "\n", na = "NA", dec = ".", row.names = FALSE, col.names = TRUE, quote = FALSE, append = TRUE)
write.table(Col_52_1_filtered, file="~/WSHPackage-master/files_Carina/Col_52_1_filtered.txt", sep="\t", eol = "\n", na = "NA", dec = ".", row.names = FALSE, col.names = TRUE, quote = FALSE, append = TRUE)

# ???
sum(is.na(pdr_chr1_Col52$PDR))
summary(pdr_chr1_Col52$PDR)
a <- table(pdr_chr1_Col52$PDR)
a[names(a)==1]
a[names(a)==0]

# ???
to.plot <- data.frame(Epipolymorphism=epipoly_chr1_Col18$Epipolymorphism, Entropy=entropy_chr1_Col18$Entropy)
to.plot <- melt(to.plot)
plot <- ggplot(to.plot,aes(x=value,y=..density..,color=variable))+geom_density()+theme_bw()
plot



# TODO: function to plot the distribution of the scores for the two samples (Violin-Plot, Heatmap, Histogram)
sample1 <- pdr_chr1_Col52 # 179481 Einträge
sample2 <- pdr$PDR # 152065 Einträge

library(plyr)
combined <- rbind.fill(sample1[c("start", "PDR")], sample2[c("start", "PDR")])


# TODO: Violin Plot
# TODO: Score gegen frequency plotten und nicht Score gegen start Position

ggplot(pdr_chr1_Col18, aes(x=PDR, y = start)) + geom_violin()
ggplot(pdr_chr1_Col52, aes(x=PDR, y = start)) + geom_violin()

test <- pdr_chr1_Col18
test$sample <- 'Col_G1_L8'
test2 <- pdr_chr1_Col52
test2$sample <- 'Col_G5_L2'
result<-rbind(test, test2)
ggplot(result, aes(x=PDR, y = start, fill=sample)) + geom_violin()

test <- fdrp_chr1_Col18
test$sample <- 'Col_G1_L8'
test2 <- fdrp_chr1_Col52
test2$sample <- 'Col_G5_L2'
result<-rbind(test, test2)
ggplot(result, aes(x=FDRP, y = start, fill=sample)) + geom_violin()

test <- qfdrp_chr1_Col18
test$sample <- 'Col_G1_L8'
test2 <- qfdrp_chr1_Col52
test2$sample <- 'Col_G5_L2'
result<-rbind(test, test2)
ggplot(result, aes(x=qFDRP, y = start, fill=sample)) + geom_violin()

test <- epipoly_chr1_Col18
test$sample <- 'Col_G1_L8'
test2 <- epipoly_chr1_Col52
test2$sample <- 'Col_G5_L2'
result<-rbind(test, test2)
ggplot(result, aes(x=Epipolymorphism, y = start, fill=sample)) + geom_violin()

test <- entropy_chr1_Col18
test$sample <- 'Col_G1_L8'
test2 <- entropy_chr1_Col52
test2$sample <- 'Col_G5_L2'
result<-rbind(test, test2)
ggplot(result, aes(x=Entropy, y = start, fill=sample)) + geom_violin()

# TODO: try to split the violing plots so that the two samples are in obe plot -> funktioniert noch nicht so ganz
GeomSplitViolin <- ggproto("GeomSplitViolin", GeomViolin, 
                           draw_group = function(self, data, ..., draw_quantiles = NULL) {
                             data <- transform(data, xminv = x - violinwidth * (x - xmin), xmaxv = x + violinwidth * (xmax - x))
                             grp <- data[1, "group"]
                             newdata <- plyr::arrange(transform(data, x = if (grp %% 2 == 1) xminv else xmaxv), if (grp %% 2 == 1) y else -y)
                             newdata <- rbind(newdata[1, ], newdata, newdata[nrow(newdata), ], newdata[1, ])
                             newdata[c(1, nrow(newdata) - 1, nrow(newdata)), "x"] <- round(newdata[1, "x"])
                             
                             if (length(draw_quantiles) > 0 & !scales::zero_range(range(data$y))) {
                               stopifnot(all(draw_quantiles >= 0), all(draw_quantiles <=
                                                                         1))
                               quantiles <- ggplot2:::create_quantile_segment_frame(data, draw_quantiles)
                               aesthetics <- data[rep(1, nrow(quantiles)), setdiff(names(data), c("x", "y")), drop = FALSE]
                               aesthetics$alpha <- rep(1, nrow(quantiles))
                               both <- cbind(quantiles, aesthetics)
                               quantile_grob <- GeomPath$draw_panel(both, ...)
                               ggplot2:::ggname("geom_split_violin", grid::grobTree(GeomPolygon$draw_panel(newdata, ...), quantile_grob))
                             }
                             else {
                               ggplot2:::ggname("geom_split_violin", GeomPolygon$draw_panel(newdata, ...))
                             }
                           })

geom_split_violin <- function(mapping = NULL, data = NULL, stat = "ydensity", position = "identity", ..., 
                              draw_quantiles = NULL, trim = TRUE, scale = "area", na.rm = FALSE, 
                              show.legend = NA, inherit.aes = TRUE) {
  layer(data = data, mapping = mapping, stat = stat, geom = GeomSplitViolin, 
        position = position, show.legend = show.legend, inherit.aes = inherit.aes, 
        params = list(trim = trim, scale = scale, draw_quantiles = draw_quantiles, na.rm = na.rm, ...))
}

ggplot(result, aes(x=PDR, y=start, fill = sample)) + geom_split_violin()



# TODO: Heatmap: but how should the heatmap look?



# TODO 
# 6. function to merge the resulting scores -> DONE
# merge PDR, FDRP, qFDRP
scores1 <- pdr
column2 <- fdrp$FDRP
column3 <- qfdrp$qFDRP

scores1$FDRP <- column2
scores1$qFDRP <- column3

write.table(scores1, file="~/WSHPackage-master/files_Carina/scores.txt", sep="\t", eol = "\n", na = "NA", dec = ".", row.names = FALSE, col.names = TRUE, quote = FALSE)

# merge Epipolymorphism, Entropy
scores2 <- epipoly
column4 <- entropy$Entropy

scores2$Entropy <- column4

write.table(scores2, file="~/WSHPackage-master/files_Carina/scores2.txt", sep="\t", eol = "\n", na = "NA", dec = ".", row.names = FALSE, col.names = TRUE, quote = FALSE)



# TODO:

combineFiles = function(filtered, score2) {
  start_time <- Sys.time()
  
  # Spalten erstellen die hinten angefügt werden sollen
  epipoly_vec <- rep(NA, nrow(filtered))
  entropy_vex <- rep(NA, nrow(filtered))
  
  lastcol <- colnames(score2)[ncol(score2)]
  
  for (i in 1:nrow(filtered)) {
    row_filtered <- filtered[i, ]
    
    score_vec <- subset(score2,(chromosome == row_filtered$chromosome) & (start >= row_filtered$V4) & (start < row_filtered$V5), select=lastcol)
    score_vec <- score_vec[!is.na(score_vec)]
    x <- mean(score_vec)
    mean_vec[i] <- x
    x <- c()
  }
  
  filtered[,lastcol] <- mean_vec 
  end_time <- Sys.time()
  end_time - start_time
  
  return(filtered)
}


plot(Col_18_1_filtered$start, Col_18_1_filtered$PDR, pch=19, cex=0.5, xlab="X", ylab="Y", main="PDR")
plot(Col_18_1_filtered$start, Col_18_1_filtered$FDRP, pch=19, cex=0.5, xlab="X", ylab="Y", main="FDRP")
plot(Col_18_1_filtered$start, Col_18_1_filtered$qFDRP, pch=19, cex=0.5, xlab="X", ylab="Y", main="qFDRP")
h <- hist(score[,ncol(score)], main = main_text, xlab = x_text, cex.main=1.0)

to.plot <- data.frame(PDR=Col_18_1_filtered$PDR, FDRP=Col_18_1_filtered$FDRP, qFDRP=Col_18_1_filtered$qFDRP)
to.plot <- melt(to.plot)
plot <- ggplot(to.plot,aes(x=value,y=..density..,color=variable))+geom_density()+theme_bw()
plot



