########################################################################################################################
## getAnnotation.R
## created: 2022-05-12
## creator: Carina Schroeder
## ---------------------------------------------------------------------------------------------------------------------
## Preparation-file for the calculation and analysis: if wanted, filter the Annotation e.g. for gene-positions, 
## and create a GRanges-object from the annotation
########################################################################################################################

#' prepare.annotation
#' 
#' function that downloads the gff file for A. thalianaa and prepares it for further use:
#'   deletes Chromosome M / Mt and C / Pt and filters for element type if parameter is existing
#' 
#' @param element states which type we want to look at further (e.g. genes). If not declared, we will look at all positions no matter which type
#' 
#' @return list containing the chromosome name, every position that is in the (pre-filtered) gff file, for example only positions in genes if 
#'   element parameter = ”gene”, and the associated strand information
#' 
#' @author Carina Schroeder
#' 
#' @noRd

prepare.annotation <- function(element){
  # library(rtracklayer) # -> to use import.gff etc.
  gff <- import.gff3("~/Data/Arabidopsis_thaliana.TAIR10.51.gff3", colnames=c("type", "ID", "source"))
  gff.data <- read.delim("~/Data/Arabidopsis_thaliana.TAIR10.51.gff3", header=F, comment.char="#")
  
  if(missing(element)){ # if no element is given, we want to look at all types / all positions
    gff.genes <- gff.data[!(gff.data$V1=="Pt" | gff.data$V1=="Mt"),] # remove "Mt" and "Pt"
    return(gff.genes)
  } else if(!(element %in% c(levels(elementMetadata(gff)[,"type"]), "intergenic positions"))){
    stop(paste("Element type",paste0("'", element, "'"),"does not exist. Please choose an element from the following list:", paste(paste0("'", c(levels(elementMetadata(gff)[,"type"]), "intergenic positions"), "'"), collapse=", ")))
  } else {
    gff.genes <- gff.data[gff.data[,3]==element,] # lines from gff3 file with only lines with element-type 
    gff.genes <- gff.genes[!(gff.genes$V1=="Pt" | gff.genes$V1=="Mt"),] # remove "Mt" and "Pt"
    return(gff.genes)
  }
}


#' get.gff.positions
#' 
#' makes a list of all positions that are e.g. in the gene or another defined element
#' 
#' @param gff.genes gff file, possibly pre-filtered by parameter element
#' 
#' @return List with chromosome name, every position that is in the (pre-filtered) gff file and strand information
#'
#' @author Carina Schroeder
#'
#' @noRd

get.gff.positions <- function(gff.genes){
  
  gff.list <- data.frame(matrix(ncol=3, nrow=0))
  #library(stringi)
  file_dir <- paste("~/WSHPackage-master/",stri_rand_strings(1, 20, pattern = "[A-Za-z0-9]"), sep="")
  write.table(paste(c("chr", "position", "strand"), collapse="\t"), file=file_dir, sep="\t", append=TRUE, col.names=FALSE, row.names=FALSE, quote = FALSE)
  
  for (i in 1:nrow(gff.genes)) { 
    
    position <- seq(from=gff.genes[i,4], to=gff.genes[i,5]-1, by=1)
    chr <- rep(gff.genes[i,1], length(position))
    strand <- rep(gff.genes[i,7], length(position))
    
    chr <- data.frame(chr)
    gff.list <- cbind(chr, position, strand)
    
    write.table(gff.list, file=file_dir, sep="\t", append=TRUE, col.names=FALSE, row.names=FALSE) # makes code faster by saving in file
    
  }
  
  gff.list <- read.table(file_dir, sep="\t", header = TRUE)
  file.remove(file_dir)
  
  return(gff.list)
  
}


#' get.annotation
#' 
#' function that concatenates annotation and gff.list
#'   
#' @param annotation the annotation that one (possibly) wants to filter. 
#' @param gff.list states which type we want to look at further (e.g. genes). If not declared, we will look at all positions no matter which type
#' @param cg_context TRUE = CpG context should be considered (both contexts = TRUE : only CG and CHG is considered)
#' @param chg_context TRUE = CHG context should be considered (both contexts = FALSE : annotation not filtered, including CHH !!!)
#' 
#' @return concatinated annotation
#' 
#' @author Carina Schroeder
#' 
#' @noRd 

get.annotation <- function(annotation, gff.list, cg_context, chg_context){ 
  
  genes_2 <- gff.list[c("chr", "position")] # don't mind strand info
  
  chr1 <- genes_2[genes_2$chr == "1",]
  chr2 <- genes_2[genes_2$chr == "2",]
  chr3 <- genes_2[genes_2$chr == "3",]
  chr4 <- genes_2[genes_2$chr == "4",]
  chr5 <- genes_2[genes_2$chr == "5",]
  
  chr11 <- chr1[!duplicated(chr1), ]
  chr22 <- chr2[!duplicated(chr2), ]
  chr33 <- chr3[!duplicated(chr3), ]
  chr44 <- chr4[!duplicated(chr4), ]
  chr55 <- chr5[!duplicated(chr5), ]
  
  merged_MA3_1 <- annotation[annotation$seqnames == "1",]
  merged_MA3_2 <- annotation[annotation$seqnames == "2",]
  merged_MA3_3 <- annotation[annotation$seqnames == "3",]
  merged_MA3_4 <- annotation[annotation$seqnames == "4",]
  merged_MA3_5 <- annotation[annotation$seqnames == "5",]
  
  concat1 <- merge(chr11, merged_MA3_1, by.x=c("chr", "position"), by.y=c("seqnames", "start"), sort = FALSE)
  concat2 <- merge(chr22, merged_MA3_2, by.x=c("chr", "position"), by.y=c("seqnames", "start"), sort = FALSE)
  concat3 <- merge(chr33, merged_MA3_3, by.x=c("chr", "position"), by.y=c("seqnames", "start"), sort = FALSE)
  concat4 <- merge(chr44, merged_MA3_4, by.x=c("chr", "position"), by.y=c("seqnames", "start"), sort = FALSE)
  concat5 <- merge(chr55, merged_MA3_5, by.x=c("chr", "position"), by.y=c("seqnames", "start"), sort = FALSE)
  
  new_df <- rbind(concat1, concat2)
  new_df <- rbind(new_df, concat3)
  new_df <- rbind(new_df, concat4)
  new_df <- rbind(new_df, concat5)
  
  if(cg_context == TRUE & chg_context == TRUE){
    new_df <- new_df[new_df$context != "CHH",] # CG, CHG
  } else if (cg_context == TRUE & chg_context == FALSE){
    new_df <- new_df[new_df$context == "CG",] # CG
  } else if (cg_context == FALSE & chg_context == TRUE){
    new_df <- new_df[new_df$context == "CHG",] # CHG
  } else {
    new_df <- new_df # CG, CHG, CHH -> alles weiter geben (!)
  }
  
  return(new_df)
  
}


#' filter.annotation
#' 
#' function that filters the annotation by downloading the gff file for A. thalianaa and preparing it for further use:
#'   deletes Chromosome M / Mt and C / Pt and filters for element type or context type if parameter was given.
#'   Also it can create the GRanges Object necessary for the Scores calculation
#' 
#' @param annotation the annotation that one (possibly) wants to filter. From this the GRanges object is created which is needed for the 
#'   calculation of the scores: PDR, FDRP, qFDRP.
#' @param element states which type we want to look at further (e.g. genes). If not declared, we will look at all positions no matter which type
#' @param cg_context TRUE = CpG context should be considered (both contexts = TRUE : only CG and CHG is considered)
#' @param chg_context TRUE = CHG context should be considered (both contexts = FALSE : annotation not filtered, including CHH !!!)
#' @param filter additional filter option (no function because criteria are not yet defined) 
#' @param make_rangesObj TRUE = create a GRanges object from the filtered annotation
#' 
#' @return filtered gff file, annotation or GRanges Object
#' 
#' @author Carina Schroeder
#' 
#' @export 

filter.annotation <- function(annotation, element, cg_context = TRUE, chg_context = TRUE, filter = FALSE, make_rangesObj = TRUE){
  
  if(missing(annotation)){
    stop(paste("Annotation missing. Please insert a file for Annotation-Parameter"))
  }
  annotation <- annotation[annotation$seqnames!="C",]
  annotation <- annotation[annotation$seqnames!="M",]
  
  if(missing(element)){ # we want to look at all positions
    # consider methylation types
    if(cg_context == TRUE & chg_context == TRUE){
      annotation <- annotation[annotation$context != "CHH",] # CG and CHG
    } else if (cg_context == TRUE & chg_context == FALSE){
      annotation <- annotation[annotation$context == "CG",] # CG only
    } else if (cg_context == FALSE & chg_context == TRUE){
      annotation <- annotation[annotation$context == "CHG",] # CHG only
    } else { # both parameter FALSE --> return unfiltered annotation with all 3 contexts CG, CHG and CHH !!!!
      annotation <- annotation
    }
    
  } else {
    
    # TODO: intergenic positions einbauen
    if(element =="intergenic positions"){
      
      # first get all gene positions
      merged_MA3_1 <- read.delim("/localscratch/c.schroeder/merged_MA3_filtered.txt", sep='\t', header=TRUE) # Annotation filtered on genomic positions
      merged_MA3_2 <- merged_MA3[merged_MA3$context=="CG",]
      merged_MA3_2 <- merged_MA3_2[!(merged_MA3_2$seqnames=="M" | merged_MA3_2$seqnames=="C"),] # Original annotation
      merged_MA3_2$seqnames <- as.integer(merged_MA3_2$seqnames)
      
      annotation <- anti_join(merged_MA3_2, merged_MA3_1, by = c("start", "seqnames")) # annotation with intergenic positions
      
    } else {
      
      gff.genes <- prepare.annotation(element)
      gff.list <- get.gff.positions(gff.genes)
      annotation <- get.annotation(annotation, gff.list, cg_context, chg_context)
      
    }
    
  }
  
  if(filter == TRUE){ 
    annotation <- annotation[annotation$Col_G1_L8>6,] # cutoff bei inklusive 6
    annotation <- annotation[c(TRUE,rep(FALSE,2)), ] # only every third position
  }
  
  if(make_rangesObj == TRUE){ # make ranges Object from annotation 
    ranges_obj <- GRanges(seqnames=annotation$seqnames, ranges=IRanges(start=annotation$start, width=nchar(annotation$context)))
    seqlevelsStyle(ranges_obj) <- 'ucsc'
    return(ranges_obj)
  }
  
  return(annotation)
  
}


# example 
annotation_genePositions <- filter.annotation(merged_MA3, element = "gene", cg_context = TRUE, chg_context = TRUE, filter = , make_rangesObj = FALSE)
